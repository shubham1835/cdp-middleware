/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.scheduledtask;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author shubhgos
 */
@Entity
public class CdpSearchedAlertPojo implements Cloneable, Serializable {

    @Column(name = "alert_reported_time")
    private String alertReportedTime;

    @Column(name = "unique_id")
    private String uniqueTrxId;

    @Id
    @Column(name = "conversation_id")
    private String conversationId;

    @Column(name = "searchable_string")
    private String errorMessage;

    public String getAlertReportedTime() {
        return alertReportedTime;
    }

    public void setAlertReportedTime(String alertReportedTime) {
        this.alertReportedTime = alertReportedTime;
    }

    public String getUniqueTrxId() {
        return uniqueTrxId;
    }

    public void setUniqueTrxId(String uniqueTrxId) {
        this.uniqueTrxId = uniqueTrxId;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
