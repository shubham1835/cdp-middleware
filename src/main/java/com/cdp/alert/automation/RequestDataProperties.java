/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.automation;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author shubhgos
 */
public class RequestDataProperties {

    private String instance;
    private String conversationId;
    private String subj;
    private String statusCode;
    private String sev;
    private String UNIQUE_ID;

    public String getUNIQUE_ID() {
        return UNIQUE_ID;
    }

    @JsonProperty("UNIQUE_ID")
    public void setUNIQUE_ID(String UNIQUE_ID) {
        this.UNIQUE_ID = UNIQUE_ID;
    }

    public String getSev() {
        return sev;
    }

    @JsonProperty("Sev")
    public void setSev(String sev) {
        this.sev = sev;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {

        this.instance = instance.substring(0, instance.indexOf("-"));
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getSubj() {
        return subj;
    }

    @JsonProperty("Subj")
    public void setSubj(String subject) {
        this.subj = subject;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
