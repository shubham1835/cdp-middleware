/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.knownalert;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author shubhgos
 */
@Entity
@Table(name = "KNOWN_ALERT")

public class KnownAlert implements Serializable {

    @Column(name = "service_name")
    private String serviceName;

    @Column(name = "status_code")
    private String statusCode;

    @Column(name = "searchable_application")
    private String searchableApplication;

    @Column(name = "namespace")
    private String nameSpace;

    @Column(name = "mail_message")
    private String mailMessage;
    
    @Id
    @Column(name = "searchable_string")
    private String errorMessage;

    @Column(name = "per_min_threshold")
    private String minThreshold;

    @Column(name = "per_hour_threshold")
    private String hrThreshold;

    @Column(name = "per_day_threshold")
    private String dayThreshold;

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getMailMessage() {
        return mailMessage;
    }

    public void setMailMessage(String mailMessage) {
        this.mailMessage = mailMessage;
    }

    public String getSearchableApplication() {
        return searchableApplication;
    }

    public void setSearchableApplication(String searchableApplication) {
        this.searchableApplication = searchableApplication;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMinThreshold() {
        return minThreshold;
    }

    public void setMinThreshold(String minThreshold) {
        this.minThreshold = minThreshold;
    }

    public String getHrThreshold() {
        return hrThreshold;
    }

    public void setHrThreshold(String hrThreshold) {
        this.hrThreshold = hrThreshold;
    }

    public String getDayThreshold() {
        return dayThreshold;
    }

    public void setDayThreshold(String dayThreshold) {
        this.dayThreshold = dayThreshold;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
