package com.cdp.alert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


//@EnableEurekaClient
@SpringBootApplication
@EnableScheduling
public class CdpAlertServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdpAlertServiceApplication.class, args);
	}

}
