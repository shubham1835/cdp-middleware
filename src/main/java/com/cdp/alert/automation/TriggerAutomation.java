/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.automation;

import com.cdp.alert.knownalert.KnownAlert;
import com.cdp.alert.searchalert.CdpAlertRepository;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author shubhgos
 */
@Component
public class TriggerAutomation {

    @Autowired
    LoginInKIBANA loginInKIBANA;
    private static final Logger logger = LoggerFactory.getLogger(TriggerAutomation.class);
    @Autowired
    CdpAlertRepository repositorycountry;

    /*  @Async
    public void startAutomation(RequestDataMapperAlerts requestDataMapperAlerts, KnownAlert knownAlert, String webHookData, String incidentKey) {
        logger.info("[Automation Start]" + knownAlert.getSearchableApplication());
        String searchableApplication = knownAlert.getSearchableApplication();
        String errorMessage = knownAlert.getErrorMessage();

        if (searchableApplication.equalsIgnoreCase("AUTOMATION")) {
            logger.info("[Kibana Automation in progress]");
            performKibanaAutomation(requestDataMapperAlerts, knownAlert, errorMessage, incidentKey);
        } else {
            logger.info("[mail Search]");
            if (webHookData.contains(errorMessage)) {
                logger.info("[mail Search]" + "success");
                successCall(requestDataMapperAlerts, knownAlert, "Mail", incidentKey);
            } else {
                logger.info("[mail Search]" + "FAIL");
                performKibanaAutomation(requestDataMapperAlerts, knownAlert, errorMessage, incidentKey);
//                failureCall(requestDataMapper, "Mail");
            }
        }
    }*/
    public Map startAutomation(RequestDataMapperAlerts requestDataMapperAlerts, KnownAlert knownAlert, String webHookData, String incidentKey) {
        logger.info("[Automation Start]" + knownAlert.getSearchableApplication());
        String searchableApplication = knownAlert.getSearchableApplication();
        String errorMessage = knownAlert.getErrorMessage();
        Map<String, KnownAlert> resultMap = new HashMap<>();
        if (searchableApplication.equalsIgnoreCase("AUTOMATION")) {
            logger.info("[Kibana Automation in progress]");
            resultMap = performKibanaAutomation(requestDataMapperAlerts, knownAlert, errorMessage, incidentKey);
        } else {
            logger.info("[mail Search]");
            if (webHookData.contains(errorMessage)) {
                logger.info("[mail Search]" + "success");
//                successCall(requestDataMapperAlerts, knownAlert, "Mail", incidentKey);
                resultMap.put("Automation Success", knownAlert);
            } else {
                logger.info("[mail Search]" + "FAIL");
                resultMap = performKibanaAutomation(requestDataMapperAlerts, knownAlert, errorMessage, incidentKey);
//                failureCall(requestDataMapper, "Mail");
            }
        }
        return resultMap;
    }

    private Map performKibanaAutomation(RequestDataMapperAlerts requestDataMapperAlerts, KnownAlert knownAlert, String errorMessage, String incidentKey) {
        Map<String, KnownAlert> resultMap = new HashMap<>();
//        loomService.loomServiceCall(requestDataMapperAlerts.getId(), incidentKey, "In Progress", "Kibana Automation in progress");
        int count = loginInKIBANA.kibanaAutomation(requestDataMapperAlerts.getProperties().getConversationId(), knownAlert.getNameSpace(), errorMessage);
        if (count == 0) {
            logger.info("[Automation]" + "FAIL");
//            failureCall(requestDataMapperAlerts, "Kibana", incidentKey);
            resultMap.put("Automation Failed", knownAlert);

        } else {
//            successCall(requestDataMapperAlerts, knownAlert, "Kibana", incidentKey);
            logger.info("[Automation]" + "SUCCESS");
            resultMap.put("Automation Success", knownAlert);
        }
        logger.info("[count]" + count);
        return resultMap;
    }

}
