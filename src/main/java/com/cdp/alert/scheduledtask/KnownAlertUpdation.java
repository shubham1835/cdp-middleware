/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.scheduledtask;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.cdp.alert.searchalert.CdpAlertRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author shubhgos
 */
@EnableAsync
@Component
public class KnownAlertUpdation {

    @Autowired
    CdpAlertRepository repositorycountry;
    @Autowired
    CdpKnownAlerUpdationRepository knownAlertUpdationServiceImpl;
    private static final Logger LOGGER = LoggerFactory.getLogger(KnownAlertUpdation.class);

    @Async
//    @Scheduled(fixedRate = 5000)
    public void scheduleFixedRateMailSearchTaskAsync() throws InterruptedException {
        LOGGER.info(
                "================================Starting scanning==============================");
//        List<CdpSearchedAlertPojo> mailSearchAlerts = countryRepository.findOpenAlertByDateForMailProcessing();
        List<CdpSearchedAlertPojo> mailSearchAlerts = knownAlertUpdationServiceImpl.findOpenAlertByDateForMailProcessing();
//        List<KnownAlert> knownAlert = knownAlertRepository.findAll();
        if (!mailSearchAlerts.isEmpty()) {
            mailSearchAlerts.stream().forEach(mailSearchedRow -> updateCdpAlertTableForMailSearch(mailSearchedRow));
        }
        LOGGER.info(
                "================================Completed scanning==============================");

//        Thread.sleep(2000);
    }

    public void updateCdpAlertTableForMailSearch(CdpSearchedAlertPojo mailSearchedRow) {
        LOGGER.info("[mailSearchedRow]" + mailSearchedRow.getConversationId());

        Integer updationCount = repositorycountry.updateMailAlert(mailSearchedRow.getConversationId(), mailSearchedRow.getUniqueTrxId(), mailSearchedRow.getErrorMessage(), "SchedulerClosed", "CL");

        LOGGER.info("[updationCount]" + updationCount);
    }
}
