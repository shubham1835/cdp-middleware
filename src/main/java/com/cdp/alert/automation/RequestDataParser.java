/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.automation;

import com.cdp.alert.knownalert.KnownAlert;
import com.cdp.alert.knownalert.KnownAlertRepository;
import com.cdp.alert.searchalert.CdpAlertRepository;
import com.cdp.alert.service.LoomService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 *
 * @author shubhgos
 */
@Component
public class RequestDataParser {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    TriggerAutomation triggerAutomation;
    @Autowired
    KnownAlertRepository knownAlertRepository;
    @Autowired
    LoomService loomService;
    @Autowired
    CdpAlertRepository repositorycountry;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RequestDataParser.class);

    @Async
    public void dataParser(String webHookData) {
        try {
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            RequestDataMapper requestDataMapper = objectMapper.readValue(webHookData, RequestDataMapper.class);
            List<RequestDataMapperAlerts> alerts = requestDataMapper.getAlerts();
//            String instance = requestDataMapper.getAlerts().get(0).getProperties().getInstance();
//            String statusCode = requestDataMapper.getAlerts().get(0).getProperties().getStatusCode();
            alerts.stream().forEach(retriveDataFromDb -> processingAlerts(retriveDataFromDb, requestDataMapper.getIncidentKey(), webHookData));

        } catch (JsonProcessingException ex) {
            Logger.getLogger(RequestDataParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void processingAlerts(RequestDataMapperAlerts requestDataMapperAlerts, String incidentKey, String webHookData) {
        RequestDataProperties requestDataProperties = requestDataMapperAlerts.getProperties();
        try {
            if (requestDataProperties.getSev().equalsIgnoreCase("Sev 4") || requestDataProperties.getSev().equalsIgnoreCase("Sev 3")) {
                archiveAlert(webHookData);
                return;
            }
        } catch (NullPointerException ex) {
            System.out.println("[processingAlerts]" + incidentKey);
        }
        List<KnownAlert> knownAlert = knownAlertRepository.findOpenAlertByDateForKibanaProcessing(requestDataProperties.getInstance(), requestDataProperties.getStatusCode());

        if (knownAlert.isEmpty()) {
            loomService.loomServiceCall(requestDataMapperAlerts.getId(), incidentKey, "Automation Failed", "Known alert data not found please handle it manually");
        }
        Map<String, KnownAlert> resultMap = new HashMap<>();
        knownAlert.stream().map(s -> triggerAutomation.startAutomation(requestDataMapperAlerts, s, webHookData, incidentKey)).parallel().forEach(printMap -> resultMap.putAll(printMap));
        logger.info("Collection of map" + resultMap.size() + "--->" + resultMap.toString());
        if (resultMap.containsKey("Automation Success")) {
            logger.info("[Success Call]");
            successCall(requestDataMapperAlerts, (KnownAlert) resultMap.get("Automation Success"), "Kibana", incidentKey);
        } else {
            logger.info("[Failure Call]");
            failureCall(requestDataMapperAlerts, "Kibana", incidentKey);
        }
    }

    private void successCall(RequestDataMapperAlerts requestDataMapperAlerts, KnownAlert knownAlert, String automation, String incidentKey) {
        try {
            List<Long> alertIds = new ArrayList<>();
            alertIds.add(requestDataMapperAlerts.getId());
            loomService.loomServiceCall(requestDataMapperAlerts.getId(), incidentKey, automation + " Automation Success", knownAlert.getMailMessage());
            loomService.loomServiceSendMailCall(requestDataMapperAlerts.getProperties().getSubj(), incidentKey, knownAlert.getMailMessage());
            loomService.loomArchiveCall(alertIds, incidentKey);
            repositorycountry.updateMailAlert(requestDataMapperAlerts.getProperties().getUNIQUE_ID(), knownAlert.getMailMessage(), "AutomationClosed", incidentKey, "CL");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("[Success exception occured]" + ex);
        }
    }

    private void failureCall(RequestDataMapperAlerts requestDataMapperAlerts, String automation, String incidentKey) {
        try {
            loomService.loomServiceCall(requestDataMapperAlerts.getId(), incidentKey, automation + " Automation Failed", "Please handle this incident manually");
            repositorycountry.updateMailAlert(requestDataMapperAlerts.getProperties().getUNIQUE_ID(), "Automation Failed", "AutomationClosed", incidentKey, "OP");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("[Failure exception occured]" + ex);
        }
    }

    public void archiveAlert(String webHookData) {
        try {
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            RequestDataMapper requestDataMapper = objectMapper.readValue(webHookData, RequestDataMapper.class);
            long countOfIncident = repositorycountry.countByincidentId(requestDataMapper.getIncidentKey());
            logger.info("[countByIncidentId]" + String.valueOf(countOfIncident));
            if (countOfIncident > 0) {
                return;
            }
            List<RequestDataMapperAlerts> alerts = requestDataMapper.getAlerts();
            alerts.stream().forEach(archiveAlert -> archiveAlertBasedOnAlertId(archiveAlert, requestDataMapper.getIncidentKey()));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(RequestDataParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void archiveAlertBasedOnAlertId(RequestDataMapperAlerts alerts, String incidentKey) {
        List<RequestDataMapperInsights> insights = alerts.getInsights();
        RequestDataProperties requestDataProperties = alerts.getProperties();
        List<Long> alertIds = new ArrayList<>();
        alertIds.add(alerts.getId());
        String insight = "Ticket with Sev > 2 closed automatically";
        boolean minorTicketCheck;
        try {
            minorTicketCheck = requestDataProperties.getSev().contains("Sev");
        } catch (NullPointerException ex) {
            minorTicketCheck = false;
        }
        Predicate<RequestDataMapperInsights> insightCheck = x -> x.getInsight().contains("Incident Closed");
        List<RequestDataMapperInsights> insightCollect = insights.stream().filter(insightCheck).collect(Collectors.toList());
        if (!insightCollect.isEmpty() || minorTicketCheck) {//"Event is manually handled"
            if (!minorTicketCheck) {
                insight = insightCollect.get(0).getRecommendations().get(0);
                loomService.loomServiceSendMailCall(alerts.getProperties().getSubj(), incidentKey, insight);
            }
            loomService.loomArchiveCall(alertIds, incidentKey);
            repositorycountry.updateMailAlert(requestDataProperties.getUNIQUE_ID(), insight, "ManuallyClosed", incidentKey, "CL");
        }
    }

}
