package com.cdp.alert.knownalert;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface KnownAlertRepository extends JpaRepository<KnownAlert, Integer> {
//WHERE to_char(cdp.alert_reported_time,'DD-MM-YY')=to_char(current_date,'DD-MM-YY')

    @Modifying
    @Query("delete from KnownAlert f where f.serviceName=?1 AND f.statusCode=?2 AND "
            + "f.searchableApplication=?3 AND f.errorMessage=?4")
    Integer deleteKnownAlert(@Param("serviceName") String serviceName, @Param("statusCode") String statusCode,
            @Param("searchableApplication") String searchableApplication, @Param("errorMessage") String errorMessage);

    @Query(value = "select a from KnownAlert a where serviceName=?1 AND statusCode=?2 AND status='OPEN'")
    public List<KnownAlert> findOpenAlertByDateForKibanaProcessing(@Param("instance") String instance, @Param("statusCode") String statusCode);

}
