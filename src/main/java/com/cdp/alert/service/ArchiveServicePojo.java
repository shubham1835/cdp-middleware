/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.service;

import java.util.List;

/**
 *
 * @author shubhgos
 */
public class ArchiveServicePojo {

    List<Long> alerts;

    public List<Long> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<Long> alerts) {
        this.alerts = alerts;
    }
}
