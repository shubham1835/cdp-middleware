/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.service;

import com.cdp.alert.automation.RequestDataMapper;
import com.cdp.alert.automation.RequestDataMapperAlerts;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 *
 * @author shubhgos
 */
@Service
public class LoomService {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    ServicePojo servicePojo;
    @Autowired
    ObjectMapper objectMapper;
    @Value("#{'${mail.of.list}'.split(',')}")
    List<String> mailIds;
    private static final Logger logger = LoggerFactory.getLogger(LoomService.class);

    public String loomCall(HttpHeaders httpHeaders) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("username", servicePojo.getUsername());
        map.add("password", servicePojo.getPassword());
        map.add("grant_type", servicePojo.getGrant_type());
        map.add("client_id", servicePojo.getClient_id());
        System.out.println("+map+" + map);
        HttpEntity<MultiValueMap> request = new HttpEntity<>(map, httpHeaders);
        ResponseEntity<TokenServicePojo> response = restTemplate.postForEntity("https://illin7026/auth/realms/Loom/protocol/openid-connect/token", request, TokenServicePojo.class);
        return response.getBody().getAccess_token();
    }

    public HttpHeaders getHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String token = loomCall(httpHeaders);
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setBearerAuth(token);
        return httpHeaders;
    }

    public HttpStatus loomServiceCall(Long alertId, String incidentId, String statusMessage, String status) {
        InsightCallPojo insightCallPojo = new InsightCallPojo();
        insightCallPojo.setAlertId(alertId);
        insightCallPojo.setIncidentId(incidentId);
        insightCallPojo.setInsight(statusMessage);
        insightCallPojo.setRecommendation(status);
        HttpEntity<InsightCallPojo> request = new HttpEntity<>(insightCallPojo, getHeader());
        ResponseEntity<String> response = restTemplate.postForEntity("https://illin7026/api/v1/insights", request, String.class);
        logger.info("[loomServiceCall response]" + response.getBody());
        return response.getStatusCode();
    }

    public HttpStatus loomArchiveCall(List<Long> alertIds, String incidentKey) {
        HttpEntity<?> request = new HttpEntity<>(getHeader());
        ResponseEntity<String> response = restTemplate.exchange("https://illin7026/api/v1/incidents/{id}/expire", HttpMethod.PUT, request, String.class, incidentKey);
        logger.info("[loomArchiveCall response]" + response.getStatusCode());
        return response.getStatusCode();
    }

    public HttpStatus loomAlertArchiveCall(List<Long> alertIds, String incidentKey) {
        ArchiveServicePojo archiveServicePojo = new ArchiveServicePojo();
        archiveServicePojo.setAlerts(alertIds);
        HttpEntity<?> request = new HttpEntity<>(archiveServicePojo, getHeader());
        ResponseEntity<String> response = restTemplate.exchange("https://illin7026/api/v1/incidents/{incidentId}/remove", HttpMethod.PUT, request, String.class, incidentKey);
        logger.info("[loomArchiveCall response]" + response.getStatusCode());
        return response.getStatusCode();
    }

    public HttpStatus loomDeleteInsightCall(long insightKey) {
        HttpEntity<?> request = new HttpEntity<>(getHeader());
        logger.info("[loomArchiveCall insightKey]" + insightKey);
        ResponseEntity<String> response = restTemplate.exchange("https://illin7026/api/v1/insights/{id}", HttpMethod.DELETE, request, String.class, insightKey);
        logger.info("[loomArchiveCall response]" + response.getStatusCode());
        return response.getStatusCode();
    }

    public HttpStatus loomServiceSendMailCall(String subject, String incidentKey, String message) {
        EmailServicePojo emailServicePojo = new EmailServicePojo();
        emailServicePojo.setEmails(mailIds);
        emailServicePojo.setMessage(message);
        emailServicePojo.setSubject(subject);
        logger.info("[loomServiceSendMailCall Subject]" + subject);
        HttpEntity<EmailServicePojo> request = new HttpEntity<>(emailServicePojo, getHeader());
        ResponseEntity<String> response = restTemplate.postForEntity("https://illin7026/api/v1/incidents/{id}/share", request, String.class, incidentKey);
        logger.info("[loomServiceSendMailCall response]" + response.getBody());
        return response.getStatusCode();
    }


    /* public void magicCall() {
        while (true) {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("appId", "AMSS");
            map.add("password", "cwpos");
            map.add("rememberMe", "false");
            map.add("username", "dummy");
            HttpEntity<MultiValueMap> request = new HttpEntity<>(map, httpHeaders);
            ResponseEntity<String> response = restTemplate.postForEntity("https://www.cricketwireless.com/selfservice/rest/authentication/login/", request, String.class);
            System.out.println("attack output" + response);
        }
    }*/
}
