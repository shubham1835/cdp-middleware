package com.cdp.alert.searchalert.description;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CdpAlertDescriptionEntity {

    @Column(name = "Alert_subject")
    private String alertSubject;

    @Column(name = "alert_reported_time")
    private String alertReportedTime;

    @Column(name = "unique_id")
    private String uniqueTrxId;

    @Column(name = "status_data")
    private String status;

    @Column(name = "reason")
    private String reason;

    @Id
    @Column(name = "conversation_id")
    private String conversationId;

    @Column(name = "kubernetes_clustername")
    private String clusterName;

    @Column(name = "status_code")
    private String statusCode;

    @Column(name = "name_space")
    private String nameSpace;

    @Column(name = "Alert_description")
    private String alertDescription;

    @Column(name = "alert_description2")
    private String alertDescription2;

    public String getAlertDescription() {
        return alertDescription;
    }

    public void setAlertDescription(String alertDescription) {
        this.alertDescription = alertDescription;
    }

    public String getAlertDescription2() {
        return alertDescription2;
    }

    public void setAlertDescription2(String alertDescription2) {
        this.alertDescription2 = alertDescription2;
    }

    public String getAlertSubject() {
        return alertSubject;
    }

    public void setAlertSubject(String alertSubject) {
        this.alertSubject = alertSubject;
    }

    public String getAlertReportedTime() {
        return alertReportedTime;
    }

    public void setAlertReportedTime(String alertReportedTime) {
        this.alertReportedTime = alertReportedTime;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getUniqueTrxId() {
        return uniqueTrxId;
    }

    public void setUniqueTrxId(String uniqueTrxId) {
        this.uniqueTrxId = uniqueTrxId;
    }

    public String getStatus() {
        if (status != null) {
            return status;
        }
        return "OPEN";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
