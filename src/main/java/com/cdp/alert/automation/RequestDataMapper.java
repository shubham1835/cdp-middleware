/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.automation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author shubhgos
 */
@Configuration
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestDataMapper {

    private String incidentKey;
    private List<RequestDataMapperAlerts> alerts;

    public String getIncidentKey() {
        return incidentKey;
    }

    public void setIncidentKey(String incidentKey) {
        this.incidentKey = incidentKey;
    }

    public List<RequestDataMapperAlerts> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<RequestDataMapperAlerts> alerts) {
        this.alerts = alerts;
    }
}
