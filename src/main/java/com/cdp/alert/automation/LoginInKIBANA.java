package com.cdp.alert.automation;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LoginInKIBANA {

    @Value("${kibana.userId}")
    String ATTID;
    @Value("${kibana.password}")
    String ATTPASSWORD;

//    static String conversationId = "6e543a92-3ef2-4d46-84bc-b8a2bb4c1db7";
//    static String Alertname = "CWPOS Alert-500 in mS OrderManagement";
    String TimeFilter = "60";

    String countofRecords = "";
    String flag = "";
    String flag2 = "";
    String flagNo = "";

    public Integer kibanaAutomation(String conversationId, String nameSpace, String ErrorMessage) {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\crickprodcdp\\Documents\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        Integer totalHitsaftersearch = 0;
        try {

            driver.navigate().to("http://ccs.web.att.com/login#?_g=()");
            Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
                    .pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);

            WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
                public WebElement apply(WebDriver driver) {
                    return driver.findElement(By.id("sg.username"));
                }
            });

            String titleofCCS = driver.getTitle();
            System.out.println("Title of CCS is: " + titleofCCS);
            if (titleofCCS.equals("Kibana")) {
                driver.findElement(By.id("sg.username")).sendKeys(ATTID);
                driver.findElement(By.id("sg.password")).sendKeys(ATTPASSWORD);
                driver.findElement(By.id("sg.login")).click();
            }

            Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
                    .pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);
            WebElement foo2 = wait2.until(new Function<WebDriver, WebElement>() {
                public WebElement apply(WebDriver driver) {
                    return driver.findElement(By.className("content"));
                }
            });

            String convId = "\"*" + conversationId + "*\" AND \"*" + ErrorMessage + "*\"  ";
            System.out.println("convId =" + convId);

            Wait<WebDriver> wait3 = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(90))
                    .pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);

            WebElement foo3 = wait3.until(new Function<WebDriver, WebElement>() {
                public WebElement apply(WebDriver driver) {

                    flag = driver
                            .findElement(By.xpath(
                                    "/html/body/div[1]/div/div/div[3]/discover-app/main/div[2]/div[2]/div/div[2]"))
                            .getAttribute("aria-hidden").toString();
                    System.out.println("flag=" + flag);

                    countofRecords = driver.findElement(By.className("discover-table-footer")).getText();
                    System.out.println("countofRecords=" + countofRecords);
                    return driver.findElement(By.className("discover-table-footer"));
                }
            });

            if (flag.equals("false")) {

                driver.findElement(By.className("ui-select-match")).click();
                /* if (Alertname.contains("CWPOS") || Alertname.contains("CWDigital ")) {
                    driver.findElement(By.xpath(
                            "/html/body/div[1]/div/div/div[3]/discover-app/main/div[2]/div[1]/disc-field-chooser/section/div[1]/div/input[1]"))
                            .sendKeys("CW-*:com-att-cw*");
                } else {
                    driver.findElement(By.xpath(
                            "/html/body/div[1]/div/div/div[3]/discover-app/main/div[2]/div[1]/disc-field-chooser/section/div[1]/div/input[1]"))
                            .sendKeys(Namespace);
                }*/
                driver.findElement(By.xpath(
                        "/html/body/div[1]/div/div/div[3]/discover-app/main/div[2]/div[1]/disc-field-chooser/section/div[1]/div/input[1]"))
                        .sendKeys(nameSpace);
                driver.findElement(By.className("ui-select-choices-group")).click();

                TimeUnit.SECONDS.sleep(20);

                driver.findElement(By.xpath(
                        "/html/body/div[1]/div/div/div[3]/discover-app/kbn-top-nav/div/div[3]/div/query-bar/form/div/div[1]/div/input"))
                        .sendKeys(convId);

                timefilterchnager(driver, TimeFilter);

                String totalHits = driver.findElement(By.xpath(
                        "/html/body/div[1]/div/div/div[3]/discover-app/kbn-top-nav/div/div[1]/div[1]/h1/span[2]"))
                        .getText();
                System.out.println("totalHits=" + totalHits);

                driver.findElement(By.xpath(
                        "/html/body/div[1]/div/div/div[3]/discover-app/kbn-top-nav/div/div[2]/div/kbn-timepicker/div/div/div[1]/div[2]/form/div[2]/button"))
                        .click();
                TimeUnit.SECONDS.sleep(30);

                driver.findElement(By.className("kuiLocalSearchButton")).click();

                Wait<WebDriver> wait4 = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(20))
                        .pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);

                WebElement foo4 = wait4.until(new Function<WebDriver, WebElement>() {
                    public WebElement apply(WebDriver driver) {

                        flag = driver
                                .findElement(By.xpath(
                                        "/html/body/div[1]/div/div/div[3]/discover-app/main/div[2]/div[2]/div/div[2]"))
                                .getAttribute("aria-hidden").toString();
                        System.out.println("flag=" + flag);

                        flagNo = driver.findElement(By.xpath(
                                "/html/body/div[1]/div/div/div[3]/discover-app/main/div[2]/div[2]/div/discover-no-results"))
                                .getAttribute("aria-hidden").toString();
                        System.out.println("flagNo=" + flagNo);

                        if (flagNo.equals("false")) {
                            return driver.findElement(By.className("discover-content"));
                        } else {
                            countofRecords = driver.findElement(By.className("discover-table-footer")).getText();
                            return driver.findElement(By.className("results"));
                        }
                    }
                });

                TimeUnit.SECONDS.sleep(10);
                totalHitsaftersearch = Integer.parseInt(driver.findElement(By.xpath(
                        "/html/body/div[1]/div/div/div[3]/discover-app/kbn-top-nav/div/div[1]/div[1]/h1/span[2]"))
                        .getText());
                System.out.println("totalHitsaftersearch=" + totalHitsaftersearch);

                if (totalHitsaftersearch == 0) {
                    System.out.println("Kindly Note We dont found any results for ConversationId.Either check by incresing Time or correct the conversation Id.");
                }
//					String RecordMsgs = driver.findElement(By.xpath(
//							"/html/body/div[1]/div/div/div[3]/discover-app/main/div[2]/div[2]/div/discover-no-results/div/div/h1"))
//							.getAttribute("innerText");
//					System.out.println("RecordMsgs=" + RecordMsgs);
                return totalHitsaftersearch;

            }
        } catch (Exception e) {
            return totalHitsaftersearch;
        } finally {
            driver.close();
            return totalHitsaftersearch;
        }

    }

    public void timefilterchnager(WebDriver driver, String time) {
        driver.findElement(By
                .xpath("/html/body/div[1]/div/div/div[3]/discover-app/kbn-top-nav/div/div[1]/div[2]/div/div/button[4]"))
                .click();
        driver.findElement(By.xpath(
                "/html/body/div[1]/div/div/div[3]/discover-app/kbn-top-nav/div/div[2]/div/kbn-timepicker/div/div/div[1]/div[1]/div/a[2]"))
                .click();
        driver.findElement(By.xpath(
                "/html/body/div[1]/div/div/div[3]/discover-app/kbn-top-nav/div/div[2]/div/kbn-timepicker/div/div/div[1]/div[2]/form/div[1]/div[1]/div[3]/div[1]/input"))
                .clear();
        driver.findElement(By.xpath(
                "/html/body/div[1]/div/div/div[3]/discover-app/kbn-top-nav/div/div[2]/div/kbn-timepicker/div/div/div[1]/div[2]/form/div[1]/div[1]/div[3]/div[1]/input"))
                .sendKeys(TimeFilter);
    }

}
