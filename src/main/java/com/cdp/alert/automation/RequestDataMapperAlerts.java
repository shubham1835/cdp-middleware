/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.automation;

import java.util.List;

/**
 *
 * @author shubhgos
 */
public class RequestDataMapperAlerts {

    private long id;
    private String patternText;
    private RequestDataProperties properties;
    private List<RequestDataMapperInsights> insights;

    public List<RequestDataMapperInsights> getInsights() {
        return insights;
    }

    public void setInsights(List<RequestDataMapperInsights> insights) {
        this.insights = insights;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPatternText() {
        return patternText;
    }

    public void setPatternText(String patternText) {
        this.patternText = patternText;
    }

    public RequestDataProperties getProperties() {
        return properties;
    }

    public void setProperties(RequestDataProperties properties) {
        this.properties = properties;
    }

}

class RequestDataMapperInsights {

    private long insightId;
    private String insight;
    private List<String> recommendations;

    public long getInsightId() {
        return insightId;
    }

    public void setInsightId(long insightId) {
        this.insightId = insightId;
    }

    public String getInsight() {
        return insight;
    }

    public void setInsight(String insight) {
        this.insight = insight;
    }

    public List<String> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<String> recommendations) {
        this.recommendations = recommendations;
    }
}
