/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdp.alert.automation.selenium;

import com.cdp.alert.automation.RequestDataMapper;
import com.cdp.alert.automation.RequestDataMapperAlerts;
import com.cdp.alert.automation.RequestDataParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author shubhgos
 */
@Configuration
public class SeleniumPropertyParser {

    @Autowired
    ObjectMapper objectMapper;

    public void seleniumDataParser(String flowElement) {
        try {
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            SeleniumDataProperty seleniumDataProperty = objectMapper.readValue(flowElement, SeleniumDataProperty.class);
            
        } catch (JsonProcessingException ex) {
            Logger.getLogger(SeleniumPropertyParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
