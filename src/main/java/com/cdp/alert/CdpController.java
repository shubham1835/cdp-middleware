package com.cdp.alert;

import com.cdp.alert.automation.RequestDataParser;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.UriComponentsBuilder;
import com.cdp.alert.searchalert.CdpAlerEntity;
import com.cdp.alert.knownalert.KnownAlert;
import com.cdp.alert.knownalert.KnownAlertRepository;
import org.springframework.web.bind.annotation.RequestParam;
import com.cdp.alert.searchalert.CdpAlertRepository;
import com.cdp.alert.searchalert.description.CdpAlertDescriptionEntity;
import com.cdp.alert.searchalert.description.CdpAlertDescriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import com.cdp.alert.searchalert.description.CdpAlertDescriptionEntity;
//import com.cdp.alert.searchalert.description.CdpAlertDescriptionRepository;

//@CrossOrigin(origins = "*")
@RestController
public class CdpController {

    @Autowired
    CdpAlertRepository countryRepository;
    @Autowired
    KnownAlertRepository knownAlertRepository;
    @Autowired
    CdpAlertDescriptionRepository cdpAlertDescriptionRepository;
    @Autowired
    RequestDataParser requestDataParser;
    private static final Logger logger = LoggerFactory.getLogger(CdpController.class);

    @RequestMapping(value = "/getAllAlert", method = RequestMethod.GET)
    public ResponseEntity<?> getAllAlerts(UriComponentsBuilder ucBuilder) {
        List<CdpAlerEntity> allAlert = countryRepository.findCountryByDate();
//        System.out.println("countries"+allAlert.get(1));
//		List<Country> countries = countryRepository.findAll();
        if (!allAlert.isEmpty()) {
            return new ResponseEntity<>(allAlert, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/getWebHookAllAlerts", method = RequestMethod.POST)
    public ResponseEntity<?> getWebHookAllAlert(@RequestBody String webHookData, UriComponentsBuilder ucBuilder) {
//        List<CdpAlerEntity> allAlert = countryRepository.findCountryByDate();

        logger.info("=============WEBHOOK MESSAGE=====================");
        logger.info(webHookData);
        requestDataParser.dataParser(webHookData);
        logger.info("=============WEBHOOK MESSAGE ENDED=====================");
        return new ResponseEntity<>(webHookData, HttpStatus.OK);
    }

    @RequestMapping(value = "/getManualArchiveAlert", method = RequestMethod.POST)
    public ResponseEntity<?> getManualArchiveAlert(@RequestBody String webHookData, UriComponentsBuilder ucBuilder) {
//        List<CdpAlerEntity> allAlert = countryRepository.findCountryByDate();

        logger.info("=============ARCHIVE MESSAGE=====================");
        logger.info(webHookData);
        requestDataParser.archiveAlert(webHookData);
        logger.info("=============ARCHIVE MESSAGE ENDED=====================");
        return new ResponseEntity<String>(webHookData, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAlertById", method = RequestMethod.GET)
    public ResponseEntity<?> getAlertDataById(@RequestParam(value = "uniqueId") String uniqueId, UriComponentsBuilder ucBuilder) {
        System.out.println("uniqueId" + uniqueId);
        List<CdpAlertDescriptionEntity> countries = cdpAlertDescriptionRepository.findAlertById(uniqueId);

//		List<Country> countries = countryRepository.findAll();
        if (!countries.isEmpty()) {
            return new ResponseEntity<List<CdpAlertDescriptionEntity>>(countries, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/closeAlertById", method = RequestMethod.POST)
    public ResponseEntity<?> closeAlertById(@RequestParam String uniqueId, @RequestParam String status, @RequestParam String ackReason, UriComponentsBuilder ucBuilder) {
        Integer closeAlert = countryRepository.closeAlertById(uniqueId, status, ackReason);
//        System.out.println("countries"+countries.get(1));
//		List<Country> countries = countryRepository.findAll();
        if (closeAlert != 0) {
            return new ResponseEntity<Integer>(closeAlert, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/getOpenAlert", method = RequestMethod.GET)
    public ResponseEntity<?> getOpenAlert(UriComponentsBuilder ucBuilder) {
        List<CdpAlerEntity> countries = countryRepository.findOpenAlertByDate();
//		List<Country> countries = countryRepository.findAll();
        if (!countries.isEmpty()) {
            return new ResponseEntity<List<CdpAlerEntity>>(countries, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<?> authCall(UriComponentsBuilder ucBuilder) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/getClosedAlert", method = RequestMethod.GET)
    public ResponseEntity<?> getClosedAlert(UriComponentsBuilder ucBuilder) {
        List<CdpAlerEntity> countries = countryRepository.findClosedAlertByDate();
//		List<Country> countries = countryRepository.findAll();
        if (!countries.isEmpty()) {
            return new ResponseEntity<List<CdpAlerEntity>>(countries, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/knownAlerts", method = RequestMethod.GET)
    public ResponseEntity<?> getKnownAlerts(UriComponentsBuilder ucBuilder) {
        List<KnownAlert> knownAlert = knownAlertRepository.findAll();
//		List<Country> countries = countryRepository.findAll();
        if (!knownAlert.isEmpty()) {
            return new ResponseEntity<List<KnownAlert>>(knownAlert, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/addKnownAlert/", method = RequestMethod.POST)
    public ResponseEntity<?> addKnownAlert(@RequestBody KnownAlert alertData, UriComponentsBuilder ucBuilder) {
//        logger.info("Creating User : {}", user);
        KnownAlert knownAlert = knownAlertRepository.save(alertData);
        return new ResponseEntity<KnownAlert>(knownAlert, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/removeKnownAlert/", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeKnownAlert(@RequestParam String serviceName,
            @RequestParam String statusCode, @RequestParam String searchableApplication,
            @RequestParam String errorMessage, UriComponentsBuilder ucBuilder) {
//        logger.info("Creating User : {}", user);
        Integer removedRows = knownAlertRepository.deleteKnownAlert(serviceName, statusCode, searchableApplication, errorMessage);
        if (removedRows == 0) {
            return new ResponseEntity<Integer>(removedRows, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Integer>(removedRows, HttpStatus.OK);
    }
}
