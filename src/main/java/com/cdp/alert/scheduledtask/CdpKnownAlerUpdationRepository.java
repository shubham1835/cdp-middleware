package com.cdp.alert.scheduledtask;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
@Transactional
public interface CdpKnownAlerUpdationRepository extends JpaRepository<CdpSearchedAlertPojo, Integer> {
//WHERE to_char(cdp.alert_reported_time,'DD-MM-YY')=to_char(current_date,'DD-MM-YY')
    @Query( value="select cdp.unique_id,cdp.conversation_id,cdp.alert_reported_time,ka.searchable_string  FROM DO_NOT_DROP_NONDOX_CDP_Alerts cdp , known_alert ka where nvl(cdp.custom_2,'OP')='OP' and SUBSTR(cdp.service_name,0,INSTR(cdp.service_name,'-')-1)=ka.service_name  and regexp_replace(cdp.status_code, '[^[:digit:]]', '')=ka.status_code and ka.searchable_application='MAIL' and to_char(cdp.alert_reported_time,'DD-MM-YY')=to_char(current_date,'DD-MM-YY') and (cdp.alert_description like '%'||ka.searchable_string||'%' or cdp.alert_description2 like '%'||ka.searchable_string||'%') and ROWNUM<10 order by cdp.alert_reported_time desc",nativeQuery =true)
    public List<CdpSearchedAlertPojo> findOpenAlertByDateForMailProcessing();

    @Query( value="select cdp.unique_id,cdp.conversation_id,cdp.alert_reported_time,ka.searchable_string  FROM DO_NOT_DROP_NONDOX_CDP_Alerts cdp , known_alert ka where nvl(cdp.custom_2,'OP')='OP' and SUBSTR(cdp.service_name,0,INSTR(cdp.service_name,'-')-1)=ka.service_name  and regexp_replace(cdp.status_code, '[^[:digit:]]', '')=ka.status_code and ka.searchable_application='MAIL' and to_char(cdp.alert_reported_time,'DD-MM-YY')=to_char(current_date,'DD-MM-YY') and (cdp.alert_description like '%'||ka.searchable_string||'%' or cdp.alert_description2 like '%'||ka.searchable_string||'%') and ROWNUM<10 order by cdp.alert_reported_time desc",nativeQuery =true)
    public List<CdpSearchedAlertPojo> findOpenAlertByDateForKibanaProcessing();

}
