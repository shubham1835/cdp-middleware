package com.cdp.alert.searchalert;

//import com.pocketutility.scheduledtask.CdpSearchedAlertPojo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface CdpAlertRepository extends JpaRepository<CdpAlerEntity, Integer> {
//WHERE to_char(cdp.alert_reported_time,'DD-MM-YY')=to_char(current_date,'DD-MM-YY')

    @Query("SELECT a FROM CdpAlerEntity a WHERE to_char(alert_reported_time,'DD-MM-YY') between to_char(current_date-1, 'DD-MM-YY') and to_char(current_date, 'DD-MM-YY') order by alert_reported_time desc")
    List<CdpAlerEntity> findCountryByDate();

    @Query("SELECT a FROM CdpAlerEntity a WHERE to_char(alert_reported_time,'DD-MM-YY') between to_char(current_date-1, 'DD-MM-YY') and to_char(current_date, 'DD-MM-YY') AND upper(nvl(custom_2,'OPEN')) !='CL' order by alert_reported_time desc")
    public List<CdpAlerEntity> findOpenAlertByDate();

    @Query("SELECT a FROM CdpAlerEntity a WHERE to_char(alert_reported_time,'DD-MM-YY') between to_char(current_date-1, 'DD-MM-YY') and to_char(current_date, 'DD-MM-YY') AND custom_2='CL' order by alert_reported_time desc")
    public List<CdpAlerEntity> findClosedAlertByDate();
//    @Query( value="select cdp.unique_id,cdp.conversation_id,cdp.alert_reported_time,ka.searchable_string  FROM cdp_alerts cdp , known_alert ka where nvl(cdp.custom_2,'OP')='OP' and SUBSTR(cdp.service_name,0,INSTR(cdp.service_name,'-')-1)=ka.service_name  and regexp_replace(cdp.status_code, '[^[:digit:]]', '')=ka.status_code and ka.searchable_application='MAIL' and to_char(cdp.alert_reported_time,'DD-MM-YY')=to_char(current_date,'DD-MM-YY') and (cdp.alert_description like '%'||ka.searchable_string||'%' or cdp.alert_description2 like '%'||ka.searchable_string||'%') and ROWNUM<10 order by cdp.alert_reported_time desc",nativeQuery =true)
//    public List<CdpSearchedAlertPojo> findOpenAlertByDateForMailProcessing();

    @Modifying
    @Query("update CdpAlerEntity c set custom_2=?5,custom_3=?4, status_data=?3, reason=?2 where UNIQUE_ID=?1")
    public Integer updateMailAlert(@Param("UNIQUE_ID") String conversationId, @Param("errorMessage") String errorMessage, @Param("statusData") String statusData, @Param("incidentKey") String incidentKey, @Param("status") String status);

    @Modifying
    @Query("update CdpAlerEntity c set status_data=?2, reason=?3 where unique_id=?1")
    public Integer closeAlertById(@Param("uniqueId") String uniqueId, @Param("status") String status, @Param("ackReason") String ackReason);

    long countByincidentId(String incidentId);

}
