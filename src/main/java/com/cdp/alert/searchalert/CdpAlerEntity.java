package com.cdp.alert.searchalert;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DO_NOT_DROP_NONDOX_CDP_Alerts")
public class CdpAlerEntity {

    @Column(name = "Alert_subject")
    private String alertSubject;

    @Column(name = "alert_reported_time")
    private String alertReportedTime;

    @Id
    @Column(name = "unique_id")
    private String uniqueTrxId;

    @Column(name = "status_data")
    private String status;

    @Column(name = "reason")
    private String reason;

    @Column(name = "conversation_id")
    private String conversationId;

    @Column(name = "kubernetes_clustername")
    private String clusterName;

    @Column(name = "status_code")
    private String statusCode;

    @Column(name = "name_space")
    private String nameSpace;

    @Column(name = "custom_2")
    private String mailSearchFlag;

    @Column(name = "custom_3")
    private String incidentId;

    public String getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(String incidentId) {
        this.incidentId = incidentId;
    }

    public String getMailSearchFlag() {
        return mailSearchFlag;
    }

    public void setMailSearchFlag(String mailSearchFlag) {
        this.mailSearchFlag = mailSearchFlag;
    }

    public String getAlertSubject() {
        return alertSubject;
    }

    public void setAlertSubject(String alertSubject) {
        this.alertSubject = alertSubject;
    }

    public String getAlertReportedTime() {
        return alertReportedTime;
    }

    public void setAlertReportedTime(String alertReportedTime) {
        this.alertReportedTime = alertReportedTime;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getUniqueTrxId() {
        return uniqueTrxId;
    }

    public void setUniqueTrxId(String uniqueTrxId) {
        this.uniqueTrxId = uniqueTrxId;
    }

    public String getStatus() {
        if (status != null) {
            return status;
        }
        return "OPEN";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
