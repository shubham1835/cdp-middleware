package com.cdp.alert.searchalert.description;

//import com.pocketutility.scheduledtask.CdpSearchedAlertPojo;
import com.cdp.alert.searchalert.*;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface CdpAlertDescriptionRepository extends JpaRepository<CdpAlertDescriptionEntity, Integer> {
//WHERE to_char(cdp.alert_reported_time,'DD-MM-YY')=to_char(current_date,'DD-MM-YY')

    @Query(value = "select Alert_subject,alert_reported_time,unique_id,status_data,reason,conversation_id,kubernetes_clustername,status_code,name_space,Alert_description,alert_description2 from CDP_ALERTS where unique_id='401'", nativeQuery = true)
    List<CdpAlertDescriptionEntity> findAlertById(String uniqueId);
}
